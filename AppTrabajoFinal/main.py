'''
Created on 09/10/2020

@author: LuVitale
'''

import sys; print('%s %s' % (sys.executable or sys.platform, sys.version))
import os; os.environ['DJANGO_SETTINGS_MODULE'] = 'TrabajoFinalDj.settings'
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from AppTrabajoFinal.models import Dispositivo, Sensor, TipoSensor, HistoriaSensor
from TrabajoFinalDj.settings import MEDIA_ROOT

def crearTipoSensor(nombre, tiene_signo, es_big_endian, valor_formula, unidad):
    tipo_sensor = TipoSensor(
        nombre = nombre,
        tiene_signo = tiene_signo,
        es_big_endian = es_big_endian,
        unidad = unidad,
        valor_formula = valor_formula
    )
    
    tipo_sensor.save()
    
    return tipo_sensor

def crearSensor(posicion, tipo, dispositivo):
    sensor = Sensor(
        posicion = posicion,
        tipo = tipo,
        dispositivo = dispositivo
    )
    
    sensor.save()
    
    return sensor

def tieneSigno(cadena):
    cadena = cadena.lower()
    
    if cadena.startswith("signed"):
        return True
    
    if cadena.startswith("unsigned"):
        return False
    
    print("Solo puede ser Unsigned short int o Signed short int.")
    return None

def esBigEndian(cadena):
    cadena = cadena.lower()
    
    if cadena.startswith("big"):
        return True
    
    if cadena.startswith("little"):
        return False
    
    print("Solo puede ser big-endian o little-endian.")
    return None

def obtenerValorDeFormula(cadena):
    pos_multiplicacion = cadena.find("*")
    if pos_multiplicacion > 0:
        return float(cadena[pos_multiplicacion + 1:])
    
    pos_division = cadena.find("/")
    if pos_division > 0:
        return 1 / float(cadena[pos_division + 1:])
    
    return 1.0

def obtenerDispositivo(nombre):
    return Dispositivo.objects.filter(nombre = nombre).first()

def obtenerTipoSensor(nombre):
    return TipoSensor.objects.filter(nombre = nombre).first()

def obtenerSensor(posicion, dispositivo):
    return Sensor.objects.filter(posicion = posicion, dispositivo = dispositivo).first()

def configurarSensor(lista, dispositivo):
    posicion = lista[0]
    nombre_tipo = lista[1]
    tipo = obtenerTipoSensor(nombre_tipo)
    if tipo == None:
        tiene_signo = tieneSigno(lista[2])
        es_big_endian = esBigEndian(lista[3])
        valor_formula = obtenerValorDeFormula(lista[4])
        unidad = lista[5]
        tipo = crearTipoSensor(nombre_tipo, tiene_signo, es_big_endian, valor_formula, unidad)
    
    sensor = obtenerSensor(posicion, dispositivo)
    if sensor == None:
        sensor = crearSensor(posicion, tipo, dispositivo)
    
    return sensor

def crearDispositivo(nombre):
    dispositivo = Dispositivo(nombre = nombre)

    dispositivo.save()

    return dispositivo

def configurarDispositivo(nombre, archivo_configuracion):
    dispositivo = obtenerDispositivo(nombre)
    if dispositivo == None:
        dispositivo = crearDispositivo(nombre)
        print(dispositivo.nombre + " creado")
    from csv import reader
    with open(archivo_configuracion, "r") as tsv_file:
        tsv_reader = reader(tsv_file, delimiter="\t")
        encabezado = next(tsv_reader)
        if encabezado != None:
            for linea in tsv_reader:
                configurarSensor(linea, dispositivo)
    
    print("Configurado " + dispositivo.nombre)
    
    return dispositivo

def cantidadDeSensoresDeDispositivo(dispositivo):
    return Sensor.objects.filter(dispositivo = dispositivo).count()

def calcularCantidadDeBytesPorDispositivo(dispositivo):
    BYTESPORSENSOR = 2
    
    return BYTESPORSENSOR * cantidadDeSensoresDeDispositivo(dispositivo) 

def dispositivoEsBigEndian(dispositivo):
    sensor = Sensor.objects.filter(dispositivo = dispositivo).first()
    return sensor.tipo.es_big_endian

def obtenerTiposDeDatosDeDispositivo(dispositivo):
    tipos_de_datos = ">" if dispositivoEsBigEndian(dispositivo) else "<"
    for sensor in Sensor.objects.filter(dispositivo = dispositivo):
        tipos_de_datos += "h" if sensor.tipo.tiene_signo else "H"
    
    return tipos_de_datos

def crearHistoriaSensor(nombre, valor, sensor):
    historia_sensor = HistoriaSensor(
        nombre = nombre,
        valor = valor,
        sensor = sensor
    )
    
    historia_sensor.save()
    
    return historia_sensor

def agregarMedicionesDeArchivo(dispositivo, ruta_archivo):
    bytes_de_dispositivo = calcularCantidadDeBytesPorDispositivo(dispositivo)
    tipos_de_datos = obtenerTiposDeDatosDeDispositivo(dispositivo)
    
    with open(ruta_archivo, "rb") as archivo:
        import struct
        registro = archivo.read(bytes_de_dispositivo)
        indice_historia = 1
        
        while registro:
            valores_medidos = struct.unpack(tipos_de_datos, registro)
            posicion_sensor = 1
            
            for valor in valores_medidos:
                nombre_historia = "Historia " + str(indice_historia)
                crearHistoriaSensor(nombre_historia, valor, obtenerSensor(posicion_sensor, dispositivo))
                print("Agregada " + nombre_historia + " de Sensor " + str(posicion_sensor) + " en " + dispositivo.nombre)
                posicion_sensor += 1
            
            indice_historia += 1
            registro = archivo.read(bytes_de_dispositivo)
    
def limpiarBD():
    Sensor.objects.all().delete()
    TipoSensor.objects.all().delete()
    Dispositivo.objects.all().delete()
    return True
    
def limpiarMedia():
    from shutil import rmtree
    rmtree(MEDIA_ROOT, ignore_errors=True)
    return True
    
if __name__ == "__main__":
    limpiarBD()
    limpiarMedia()
    
    configurarDispositivo("Dispositivo 1", "test/configuration/tabla1.tsv")
    configurarDispositivo("Dispositivo 2", "test/configuration/tabla2.tsv")
    
    agregarMedicionesDeArchivo(obtenerDispositivo("Dispositivo 1"), "test/files/device1.dat")
    agregarMedicionesDeArchivo(obtenerDispositivo("Dispositivo 2"), "test/files/device2.dat")