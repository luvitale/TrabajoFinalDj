from django.db import models
from django.db.models.deletion import CASCADE

# Create your models here.
class Dispositivo(models.Model):
    nombre = models.CharField(verbose_name="Nombre del dispositivo", max_length=25, default="Sin nombre", null=False, primary_key=True)
    
    def __str__(self):
        return self.nombre
    
class TipoSensor(models.Model):
    nombre = models.CharField(verbose_name="Nombre de categoría", max_length=25, default="Sin nombre", null=False, primary_key=True)
    tiene_signo = models.BooleanField(verbose_name="¿Tiene signo?", default=True, null=False)
    es_big_endian = models.BooleanField(verbose_name="¿Es Big Endian?", default=True, null=False)
    valor_formula = models.DecimalField(verbose_name="Valor de la fórmula", max_digits=15, decimal_places=8, default=1.0, null=False)
    unidad = models.CharField(verbose_name="Unidad", max_length=10, default="Sin unidad", null=False)
    
    def __str__(self):
        return self.nombre
    
class Sensor(models.Model):
    posicion = models.IntegerField(verbose_name="Posición del sensor", default=0, null=False)
    tipo = models.ForeignKey(to=TipoSensor, on_delete=CASCADE, related_name="sensores")
    dispositivo = models.ForeignKey(to=Dispositivo, on_delete=CASCADE, related_name="sensores")
    
    class Meta:
        managed = True
        db_table = "sensor"
        unique_together = (("posicion", "dispositivo"))
    
    def __str__(self):
        return str(self.posicion) + " " + self.tipo.nombre + " " + self.dispositivo.nombre
    
class HistoriaSensor(models.Model):
    nombre = models.CharField(verbose_name="Nombre de la historia", max_length=25, default="Sin nombre", null=False)
    valor = models.DecimalField(verbose_name="Valor registrado", max_digits=15, decimal_places=8, default=0, null=False)
    sensor = models.ForeignKey(to=Sensor, on_delete=CASCADE, related_name="valores")

    def __str__(self):
        return self.nombre + " " + str(self.valorIngenieria()) + " " + self.sensor.tipo.unidad
    
    def valorIngenieria(self):
        return self.valor * self.sensor.tipo.valor_formula