from django.contrib import admin
from AppTrabajoFinal.models import Dispositivo, Sensor, TipoSensor, HistoriaSensor
from django.utils.html import format_html

# Register your models here.
class DispositivoAdmin(admin.ModelAdmin):
    pass

admin.site.register(Dispositivo, DispositivoAdmin)

class SensorAdmin(admin.ModelAdmin):
    list_display = ("sensor", "obtener_nombre_dispositivo", "obtener_tipo_sensor")
    
    def sensor(self, obj):
        return "Sensor " + str(obj.posicion)
    
    sensor.short_description = "Sensor"
    
    def obtener_nombre_dispositivo(self, obj):
        return obj.dispositivo.nombre
    
    obtener_nombre_dispositivo.short_description = "Nombre del dispositivo"
    
    def obtener_tipo_sensor(self, obj):
        return obj.tipo.nombre
    
    obtener_tipo_sensor.short_description = "Tipo de sensor"

admin.site.register(Sensor, SensorAdmin)

class TipoSensorAdmin(admin.ModelAdmin):
    pass

admin.site.register(TipoSensor, TipoSensorAdmin)

class HistoriaSensorAdmin(admin.ModelAdmin):
    list_display = (
        "nombre",
        "obtener_valor_ingenieria",
        "obtener_nombre_dispositivo",
        "obtener_sensor",
        "obtener_tipo_sensor"
    )
    
    def obtener_valor_ingenieria(self, obj):
        return str(obj.valorIngenieria()) + " " + obj.sensor.tipo.unidad
    
    def obtener_nombre_dispositivo(self, obj):
        return self.colorear_contenido(obj.sensor.dispositivo.nombre)

    obtener_nombre_dispositivo.short_description = "Nombre del dispositivo"
    
    def obtener_sensor(self, obj):
        return self.colorear_contenido("Sensor " + str(obj.sensor.posicion))
    
    obtener_sensor.short_description = "Sensor"
    
    def obtener_tipo_sensor(self, obj):
        return self.colorear_contenido(obj.sensor.tipo)
    
    obtener_tipo_sensor.short_description = "Tipo del sensor"
    

    def colorear_contenido(self, data):
        return format_html(
            '<span style="color: #00b347;"> {}</span>',
            data,
        )

admin.site.register(HistoriaSensor, HistoriaSensorAdmin)