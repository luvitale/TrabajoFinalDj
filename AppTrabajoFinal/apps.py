from django.apps import AppConfig
import os
from TrabajoFinalDj.settings import BASE_DIR

APP_NAME = "AppTrabajoFinal"

APP_DIR = os.path.join(BASE_DIR, APP_NAME)

class ApptrabajofinalConfig(AppConfig):
    name = APP_NAME
